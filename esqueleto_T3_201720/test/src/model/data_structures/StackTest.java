package model.data_structures;

import static org.junit.Assert.*;



import org.junit.Test;

public class StackTest {
	
	private Stack stack;
	
	public void escenario1()
	{
		stack = new Stack();
		
	}

	public void escenario2()
	{
		stack = new Stack();	
		
		for(int i=0; i<5; i++){
			stack.push(i+1);
		}
	}
	public void escenario3()
	{
		stack = new Stack();
		for(int i=0; i<10; i++){
			stack.push(i+1);
		}
	}
	public void escenario4()
	{
		stack = new Stack();
	}

	@Test
	public final void testStack() {
		escenario1();

		assertNull("el atributo first debe ser null", stack.getFirst());
		assertEquals("el atributo n debe ser igual a 0",0, stack.getSize() );
		assertNotNull("el atributo iter debe no debe de ser null", stack.getIter());
		
	}

	@Test
	public final void testIsEmpty() {
		escenario2();

	assertTrue("el metodo debe retornar false",  stack.isEmpty()==false);
	}

	@Test
	public final void testSize() {
		escenario2();
	assertEquals("el tama�o de la pila debe ser igual a 5",5, stack.getSize() );
	
	stack.push("vale verga la vida"); 
	double[] r = new double[5];	
	stack.push(r);
	
	assertEquals("el tama�o de la pila debe ser igual a 7",7, stack.getSize() );
	for(int i=0; i< 100; i++){
		stack.push(i+3);
	}
	
	assertEquals("el tama�o de la pila debe ser igual a 100",107, stack.getSize() );

	
	}

	@Test
	public final void testPop() {
	 escenario1();
	 stack.push(17); stack.push(18); stack.push(19); stack.push(11); stack.push("nn");
	stack.push(12); stack.push(13); stack.push(14); stack.push(15); stack.push(16);
		
	 assertEquals("el item del nodo eliminado debe ser igual al entero 16",16, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 15",15, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 14",14, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 13",13, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 12",12, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 16","nn", stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 15",11, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 14",19, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 13",18, stack.pop() );
	 assertEquals("el item del nodo eliminado debe ser igual al entero 12",17, stack.pop() );
	 
	
	 
	}

	@Test
	public final void testPeek() {
		escenario1();
		
	 stack.push("prueba1");
	 
	 assertEquals("el item del nodo dbe ser igual a la cadena: prueba1", "prueba1", stack.peek());
	 
	 stack.push(245);
	 
	 assertEquals(" el item del nodo debe ser igual al entero 245", 245, stack.peek());
	}

	

	@Test
	public final void testPush() {
		
		escenario1();
		stack.push("prueba1");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba1",stack.peek() );
		
		stack.push("prueba2");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba2",stack.peek() );
		
		stack.push("prueba3");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba3",stack.peek() );
		
		stack.push("prueba4");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba4",stack.peek() );
		
		stack.push("prueba5");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba5",stack.peek() );
		
		stack.push("prueba6");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba6",stack.peek() );
		
		stack.push("prueba7");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba7",stack.peek() );
		
		stack.push("prueba8");
		assertEquals("el item del nodo creado debe ser igual a la cadena: prueba1", "prueba8",stack.peek() );
		
		
	}



	
	
}
