package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class QueueTest extends TestCase{

	Queue<String> q;
	@Before
	private void setupScenario1()
	{
		q= new Queue<String>();
	}
	private void setupScenario2()
	{
		q= new Queue<String>();
		String one= "one";
		String two= "two";
		q.enqueue(one);
		q.enqueue(two);
	}
	@Test
	public void testQueue() {
		setupScenario1();
		assertEquals("Size deberia ser cero",0, q.size());
	}
	@Test
	public void testEnqueue()
	{
		setupScenario1();
		String one= "one";
		String two= "two";
		q.enqueue(one);
		assertEquals("El tamanho deberia ser 1", 1,q.size());
		q.enqueue(two);
		assertEquals("El tamanho deberia ser 2", 2,q.size());
		assertEquals( "El item menos reciente agrgado deberia ser one no two",one ,q.peek());
	}
	public void testDequeue()
	{
		setupScenario2();
		q.dequeue();
		assertEquals("El tamanho deberia ser 1", 1,q.size());
		q.dequeue();
		assertTrue("Size deberia ser cero", q.isEmpty() );
		try
		{
			q.dequeue();
			fail("No exception");
		}
		catch (Exception e)
		{
			assertTrue(e.getMessage().equals("Queue underflow"));
		}
	}
}
