package model.data_structures;


	import java.util.Iterator;
	import java.util.NoSuchElementException;

	public class Stack<Item> implements IStack {
	    private Node<Item> first;     // top of stack
	    private int n;       
	    private Iterator<Item> iter;// size of the stack

	    // helper linked list class
	    private static class Node<Item> {
	        private Item item;
	        private Node<Item> next;
	    }

	    /**
	     * Initializes an empty stack.
	     */
	    public Stack() {
	        first = null;
	        n = 0;
	        iter = new ListIterator() ;
	    }

	    public Iterator getIter(){
			return iter;
		}
	    
	    public  Node getFirst(){
	    	return first;
	    }
	    /**
	     * Returns true if this stack is empty.
	     *
	     * @return true if this stack is empty; false otherwise
	     */
	    public boolean isEmpty() {
	        return first == null;
	    }

	   
	    public int getSize() {
	        return n;
	    }

	  

	    /**
	     * Removes and returns the item most recently added to this stack.
	     *
	     * @return the item most recently added
	     * @throws NoSuchElementException if this stack is empty
	     */
	    public Item pop() {
	        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
	        Item item = first.item;        // save item to return
	        first = first.next;            // delete first node
	        n--;
	        
	        return item;                   // return the saved item
	    }


	    /**
	     * Returns (but does not remove) the item most recently added to this stack.
	     *
	     * @return the item most recently added to this stack
	     * @throws NoSuchElementException if this stack is empty
	     */
	    public Item peek() {
	        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
	        return first.item;
	    }

	       

	    /**
	     * Returns an iterator to this stack that iterates through the items in LIFO order.
	     *
	     * @return an iterator to this stack that iterates through the items in LIFO order
	     */
	    public Iterator<Item> iterator() {
	        return new ListIterator();
	    }

	    // an iterator, doesn't implement remove() since it's optional
	    private class ListIterator implements Iterator<Item> {
	        private Node<Item> current= first;

	        public boolean hasNext() {
	            return current != null;
	        }

	        public Item next() {
	            if (!hasNext()) throw new NoSuchElementException();
	            Item item = current.item;
	            current = current.next; 
	            return item;
	        }

			@Override
			public void remove() {}
	    }
	    
	  

		@Override
		public void push(Object item) {
			
			   Node<Item> oldfirst = first;
		        first = new Node<Item>();
		        first.item = (Item) item;
		        first.next = oldfirst;
		        n++;
		}
}
