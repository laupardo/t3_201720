package model.data_structures;

public interface IStack<Item> extends Iterable<Item> {

	public void push (Item item);
	
	public Item pop();
}
