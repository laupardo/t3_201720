package model.data_structures;




public interface IQueue<E> extends Iterable<E>{

	
	public void enqueue(E item);
	
	public E dequeue();

}
