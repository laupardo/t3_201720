package model.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BusUpdateVO {


	int vehicleNo;
	int tripId;
	int routeNo;
	String direction;
	String destination;
	String pattern;
	double latitude;
	double longitude;
	Date recordedTime;
	String routeMap;
	//"Href":
	public BusUpdateVO(int pVehicleNo, int pTripId,int pRouteNo,String pDirection,String pDestination,String pPattern,double pLatitude,
			double pLongitude,String pRecordedTime,String pRouteMap)
	{
		vehicleNo=pVehicleNo;
		tripId=pTripId;
		routeNo=pRouteNo;
		direction=pDirection;
		destination=pDestination;
		pattern=pPattern;
		latitude=pLatitude;
		longitude=pLongitude;
		DateFormat df = new SimpleDateFormat("HH:mm:ss aaa"); 
		try {
			recordedTime=df.parse(pRecordedTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		routeMap=pRouteMap;

	}

}
