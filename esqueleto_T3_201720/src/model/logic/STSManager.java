package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	Queue<BusUpdateVO> queue= new Queue<BusUpdateVO>();
	@Override
	public void readBusUpdate(File rtFile) 
	{
		try 
		{
			BufferedReader in = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			BusUpdateVO[] updates= gson.fromJson(in, BusUpdateVO[].class);
			
			for(int i=0;i<updates.length;i++)
			{
				queue.enqueue(updates[i]);
			}
		
			in.close();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}


	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadStops() {
		String stopsFile="file";
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String line = br.readLine();
			while (line!=null)
			{
				//split
			}
			br.close();

		}
		catch(Exception e)
		{

		}

	}
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{

		final int R = 6371*1000; // Radious of the earth

		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;

		return distance;

	}
	private double toRad(Double value) {
		return value * Math.PI / 180;
	}

}
